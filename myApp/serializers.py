from rest_framework import serializers
from .models import Funcionario, Setor


class FuncionarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Funcionario
        fields = ['id', 'nome', 'data_nascimento']

class SetorSerializer(serializers.ModelSerializer):
    funcionarios = FuncionarioSerializer(many=True)

    class Meta:
        model = Setor
        fields = ['id', 'nome', 'funcionarios']
