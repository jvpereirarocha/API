from django.urls import path, include
from .views import FuncionarioViewSet, SetorViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register('funcionarios', FuncionarioViewSet, basename='funcionarios')
router.register('setores', SetorViewSet, basename='setores')

app_name = 'api'

urlpatterns = [
    path('', include(router.urls)),
]
