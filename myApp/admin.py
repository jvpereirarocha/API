from django.contrib import admin
from .models import Funcionario, Setor


class FuncionarioAdmin(admin.ModelAdmin):
    model = Funcionario
    list_display = ['nome', 'data_nascimento']
    search_fields = ['nome']

    class Meta:
        verbose_name = 'Api'
        verbose_name_plural = "API's"


admin.site.register(Funcionario, FuncionarioAdmin)
admin.site.register(Setor)
