from rest_framework import viewsets, status
from django.http import HttpResponse
from .models import Funcionario, Setor
from .serializers import FuncionarioSerializer, SetorSerializer
from rest_framework.response import Response


class FuncionarioViewSet(viewsets.ModelViewSet):
    serializer_class = FuncionarioSerializer

    def get_queryset(self):
        return Funcionario.objects.all()

    def list(self, request, *args, **kwargs):
        serializer = FuncionarioSerializer(self.get_queryset(), many=True)
        return Response(serializer.data)

    def create(self, request):
        func = Funcionario.objects.create(nome=request.data['nome'], data_nascimento=request.data['data_nascimento'])
        print(request.data)
        serializer = FuncionarioSerializer(func)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Exception:
            return HttpResponse("Não foi possível deletar")

        return Response(status=status.HTTP_204_NO_CONTENT)


class SetorViewSet(viewsets.ModelViewSet):
    serializer_class = SetorSerializer

    def get_queryset(self):
        return Setor.objects.all()

    def list(self, request):
        serializer = SetorSerializer(self.get_queryset(), many=True)
        return Response(serializer.data)

    # def create(self, request):
    #     setor = Setor.objects.create(nome=request.data['nome'], funcionarios=request.data['funcionarios'])
    #     print(request.data)
    #     serializer = SetorSerializer(setor)
    #     return Response(serializer.data)

    def retrieve(self, request, pk=None):
        setor = Setor.objects.filter()
        print(setor)
        serializer = SetorSerializer(setor)
        return Response(serializer.data)
