# Generated by Django 2.1.5 on 2019-02-04 13:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myApp', '0002_setor_funcionarios'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='funcionario',
            options={'verbose_name': 'Funcionários', 'verbose_name_plural': 'Lista de Funcionários'},
        ),
        migrations.AlterModelOptions(
            name='setor',
            options={'verbose_name': 'Setores', 'verbose_name_plural': 'Lista de Setores'},
        ),
    ]
