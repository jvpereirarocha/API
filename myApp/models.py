from django.db import models


class Funcionario(models.Model):
    nome = models.CharField(max_length=255)
    data_nascimento = models.DateField(blank=False, null=False)
    setor = models.ForeignKey('Setor', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        verbose_name = 'Funcionários'
        verbose_name_plural = 'Lista de Funcionários'

    def __str__(self):
        return "{} | {}".format(self.nome, self.data_nascimento.strftime('%d/%m/%Y'))


class Setor(models.Model):
    codigo = models.IntegerField()
    nome = models.CharField(max_length=255)
    class Meta:
        verbose_name = 'Setores'
        verbose_name_plural = 'Lista de Setores'

    def __str__(self):
        return "Setor: {}  ||  Nº funcionarios: {}".format(self.nome, self.funcionarios.count())
